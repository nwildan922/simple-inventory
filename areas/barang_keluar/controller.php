<?php
    include 'repositories/unit_of_work.php';
    
    $barang_keluar_repository = new barang_keluar_repository();
    $title ='Input Barang Keluar';
    $connection = mysqli_connect($DbServer, $DbUser, $DbPassword,$DbName);
    $CurrentUrl = $BaseUrl.'?page=barang';
    // initialize data
    $id = '';
    $tanggal = date('Y-m-d');
    $penerima = '';

    if (isset($_GET['action'])) {
        $total = 0;
        $bayar = 0;
        $kembalian = 0;
        $action = strtolower($_GET['action']);
        if($action == 'add') {
            $selected_id = 0;
            include 'view/submit.php';
        }
        if($action == 'view') {
            $selected_id = $_GET['id'];
            include 'view/submit.php';
        }
        if($action == 'delete'){
            if(isset($_GET['id'])){
                $id = $_GET['id'];
                $result = $barang_keluar_repository->delete($id);  
                if($result)
                    echo GetMessage('hapus',$CurrentUrl);
                else
                    echo GetErrorMessage();          
            }
        }
    }else{
        $result = $barang_keluar_repository->get_data();       
        include 'view/index.php';
    }
?>