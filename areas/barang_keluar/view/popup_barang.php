<?php
  $query = "SELECT b.id, b.kode_barang, b.nama_barang, b.jenis_barang, b.harga, SUM(sf.kuantiti) AS kuantiti
          FROM barang b
          JOIN stok_fifo sf ON b.id = sf.id_barang
          GROUP by b.id, b.kode_barang, b.nama_barang, b.jenis_barang, b.harga;";
  $resultBarang = mysqli_query($connection,$query);  
?>
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Data Barang</h4>
      </div>
      <div class="modal-body">
        <table id='table' class="table table-bordered table-stripped">
          <thead>
            <tr>
              <th>No</th>
              <th>Kode Barang</th>
              <th>Nama Barang</th>
              <th>Kuantiti</th>
              <th>Harga</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php
                $no = 1;
                while($row = mysqli_fetch_array($resultBarang,MYSQLI_ASSOC)){
                    $id = $row['id'];
                    $kodebarang = $row['kode_barang'];
                    $namabarang = $row['nama_barang'];
                    $kuantiti = $row['kuantiti'];
                    $harga = $row['harga'];
                    echo '<tr>'.
                            '<td>'.$no.'</td>'.
                            '<td>'.$kodebarang.'</td>'.
                            '<td>'.$namabarang.'</td>'.
                            '<td>'.$kuantiti.'</td>'.
                            '<td>'.$harga.'</td>'.
                            '<td><button type="button" class="btn btn-sm btn-primary" onclick="onPilih('.$id.',\''.$kodebarang.'\',\''.$namabarang.'\',\''.$kuantiti.'\',\''.$harga.'\')">Pilih</button></td>'.
                        '</tr>';
                    $no++;
                }
            ?>
          </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<script>
  $('#table').DataTable();
  function onPilih(id,kode_barang,nama_barang,kuantiti,harga){
    debugger;
    let number = $('#data_tbl_barang_keluar tr').length;
    const model = {
      "row": number,
      "id":id,
      "kode_barang":kode_barang,
      "nama_barang":nama_barang,
      "kuantiti":1,
      "stok_kuantiti":kuantiti,
      "harga":harga,
      "subtotal":harga * 1,
    };
    let existingData = sessionStorage.getItem('barang-keluar');
    if(existingData === undefined || existingData === null || existingData === "" || existingData === "[]"){
      let temp = [];
      temp.push(model);
      let data = JSON.stringify(temp);
      sessionStorage.setItem('barang-keluar',data);
      bindData();        
      alert('Data berhasil ditambahkan');
    }else{
      let temp = JSON.parse(existingData);
      let isAlreadyExist = false;

      for(var i=0;i<temp.length;i++){
        if(temp[i].id === model.id){
          isAlreadyExist = true;
          break;
        }
      } 
      
      if(!isAlreadyExist){
        temp.push(model);
        let data = JSON.stringify(temp);
        sessionStorage.setItem('barang-keluar',data);
        bindData();        
        alert('Data berhasil ditambahkan');
      }else{
        alert('Jenis barang sudah dipilih');        
      }
    }
  }
</script>