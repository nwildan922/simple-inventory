<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="card-title">
                <div class="title">List Data Barang Keluar</div>
                </div>
            </div>
            <div class="panel-body">
                <a href='?page=<?php echo $currentPage ?>&action=add' class='btn btn-primary btn-tambah'>Tambah</a>
                <table id='table' class="table table-bordered table-striped">
                    <thead>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Petugas</th>
                        <th>Total Barang Keluar</th>
                        <th>Total Nominal</th>
                        <th>Aksi</th>
                    </thead>
                    <tbody>
                        <?php
                            $no = 1;
                            while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){
                                $id = $row['id'];
                                $tanggal = $row['tanggal'];
                                $id_pengguna = $row['nama_lengkap'];
                                $total_barang_keluar = $row['total_barang_keluar'];
                                $total_nominal = $row['total_nominal'];
                                echo '<tr>'.
                                        '<td>'.$no.'</td>'.
                                        '<td>'.$tanggal.'</td>'.
                                        '<td>'.$id_pengguna.'</td>'.
                                        '<td>'.$total_barang_keluar.'</td>'.
                                        '<td>'.$total_nominal.'</td>'.
                                        '<td><a href="?page='.$currentPage.'&action=view&id='.$id.'" class="btn btn-primary btn-sm">Lihat</a> | <button class="btn btn-danger btn-sm" onclick="onDelete('.$id.')">Hapus</button></td>'.
                                    '</tr>';
                                $no++;
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    function onDelete(id){
        var result = confirm('Apakah anda yakin akan menghapus data ?');
        if(result){
            window.location.href = `?page=<?php echo $currentPage ?>&action=delete&id=${id}`;
        }else{
            alert('Proses hapus digagalkan');
        }
    }
</script>