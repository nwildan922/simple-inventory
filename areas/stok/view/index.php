<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="card-title">
                <div class="title">List Data Stok Barang</div>
                </div>
            </div>
            <div class="panel-body">
                <table id='table' class="table table-bordered table-striped">
                    <thead>
                        <th>No</th>
                        <th>Kode Barang</th>
                        <th>Nama Barang</th>
                        <th>Kuantiti</th>
                    </thead>
                    <tbody>
                        <?php
                            $no = 1;
                            while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){
                                $kode_barang = $row['kode_barang'];
                                $nama_barang = $row['nama_barang'];
                                $kuantiti = $row['kuantiti'];
                                echo '<tr>'.
                                        '<td>'.$no.'</td>'.
                                        '<td>'.$kode_barang.'</td>'.
                                        '<td>'.$nama_barang.'</td>'.
                                        '<td>'.$kuantiti.'</td>'.
                                    '</tr>';
                                $no++;
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>

    function onDelete(id){
        var result = confirm('Apakah anda yakin akan menghapus data ?');
        if(result){
            window.location.href = `?page=<?php echo $currentPage ?>&action=delete&id=${id}`;
        }else{
            alert('Proses hapus digagalkan');
        }
    }
</script>