<?php
    include 'repositories/unit_of_work.php';
    
    $supplier_repository = new supplier_repository();
    $title ='Master Supplier';
    $connection = mysqli_connect($DbServer, $DbUser, $DbPassword,$DbName);
    $currentPage = 'supplier';
    $CurrentUrl = $BaseUrl.'?page='.$currentPage;

    if (isset($_GET['action'])) {
        $action = strtolower($_GET['action']);
        // initialize model
        $id = '0';
        $nama = '';
        $alamat = '';
        $no_hp = '';
        $email = '';
        
        if($action == 'add') {
            include 'view/submit.php';
        }

        if($action == 'edit') {
            if(isset($_GET['id'])){
                $id = $_GET['id'];
                $result = $supplier_repository->get_by_id($id);
                $data = mysqli_fetch_array($result,MYSQLI_ASSOC);
                $nama = $data['nama_supplier'];
                $alamat = $data['alamat'];
                $no_hp = $data['no_hp'];
                $email = $data['email'];
            }
            include 'view/submit.php';
        }
        
        if($action == 'submit') {

            if(isset($_POST['id'])){
                $id = $_POST['id'];
                $nama = $_POST['nama'];
                $alamat = $_POST['alamat'];
                $no_hp = $_POST['no_hp'];
                $email = $_POST['email'];

                if($id == 0){
                    $result = $supplier_repository->add($nama,$alamat,$no_hp,$email);
                    if($result)
                        echo GetMessage('simpan',$CurrentUrl);
                    else
                        echo GetErrorMessage();   
                }else{
                    $result = $supplier_repository->add($id,$nama,$alamat,$no_hp,$email);
                    if($result)
                        echo GetMessage('simpan',$CurrentUrl);         
                    else
                        echo GetErrorMessage();
                }
            }
        }

        if($action == 'delete'){
            if(isset($_GET['id'])){
                $id = $_GET['id'];
                $result = $supplier_repository->delete($id);
                if($result)
                    echo GetMessage('hapus',$CurrentUrl);
                else
                    echo GetErrorMessage();          
            }
        }
    }else{
        $result = $supplier_repository->get_all();
        include 'view/index.php';
    }
?>