<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="card-title">
                <div class="title">List Data Supplier</div>
                </div>
            </div>
            <div class="panel-body">
                <a href='?page=<?php echo $currentPage ?>&action=add' class='btn btn-primary btn-tambah'>Tambah</a>
                <table id='table' class="table table-bordered table-striped">
                    <thead>
                        <th>No</th>
                        <th>Nama Supplier</th>
                        <th>Alamat</th>
                        <th>No Handphone</th>
                        <th>Email</th>
                        <th>Aksi</th>
                    </thead>
                    <tbody>
                        <?php
                            $no = 1;
                            while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){
                                $id = $row['id'];
                                $nama = $row['nama_supplier'];
                                $alamat = $row['alamat'];
                                $no_hp = $row['no_hp'];
                                $email = $row['email'];
                                echo '<tr>'.
                                        '<td>'.$no.'</td>'.
                                        '<td>'.$nama.'</td>'.
                                        '<td>'.$alamat.'</td>'.
                                        '<td>'.$no_hp.'</td>'.
                                        '<td>'.$email.'</td>'.
                                        '<td><a href="?page='.$currentPage.'&action=edit&id='.$id.'"class="btn btn-warning btn-sm ">Edit</a>|<button class="btn btn-danger btn-sm" onclick="onDelete('.$id.')">Hapus</button></td>'.
                                    '</tr>';
                                $no++;
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script src='/inventori/assets/js/pages/supplier/index.js'></script>