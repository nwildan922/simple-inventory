<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="card-title">
                <div class="title">Input Data</div>
                </div>
            </div>
            <div class="panel-body">
                <form class="col-md-12" action='?page=<?php echo $currentPage ?>&action=submit' method='post'>
                    <input type='hidden' name='id' value='<?php echo $id ?>'/>
                    <div class="form-group">
                        <label>Nama Supplier</label>
                        <input type="text" class="form-control" name='nama' value='<?php echo $nama ?>' required>
                    </div>
                    <div class="form-group">
                        <label>Alamat </label>
                        <input type="text" class="form-control" name='alamat' value='<?php echo $alamat ?>' required>
                    </div>
                    <div class="form-group">
                        <label>No Handphone</label>
                        <input type="number" class="form-control" name='no_hp' value='<?php echo $no_hp ?>' required>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" class="form-control" name='email' value='<?php echo $email ?>' required>
                    </div>
                    <button type="submit" class="btn btn-success">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>