<div class="row">
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="board">
            <div class="panel panel-primary">
            <div class="number">
                <h3>
                    <h3><?php echo $total_barang_masuk ?></h3>
                    <small>Barang Masuk</small>
                </h3>
            </div>
            <div class="icon">
                <i class="fa fa-sign-in fa-5x red"></i>
            </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="board">
            <div class="panel panel-primary">
            <div class="number">
                <h3>
                    <h3><?php echo $total_penjualan ?></h3>
                    <small>Penjualan</small>
                </h3>
            </div>
            <div class="icon">
                <i class="fa fa-shopping-cart fa-5x blue"></i>
            </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="board">
            <div class="panel panel-primary">
            <div class="number">
                <h3>
                    <h3><?php echo $total_pendapatan_perhari ?></h3>
                    <small>Pendapatan Harian</small>
                </h3>
            </div>
            <div class="icon">
                <i class="fa fa-money fa-5x green"></i>
            </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="board">
            <div class="panel panel-primary">
            <div class="number">
                <h3>
                    <h3><?php echo $total_pendapatan_perbulan ?></h3>
                    <small>Pendapatan Bulanan</small>
                </h3>
            </div>
            <div class="icon">
                <i class="fa fa-money fa-5x yellow"></i>
            </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-9 col-sm-12 col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">Penjualan 5 Tahun Terakhir</div>
            <div class="panel-body">
                <div id="morris-line-chart"></div>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">Jenis Barang Terjual</div>
            <div class="panel-body">
                <div id="morris-donut-chart"></div>
            </div>
        </div>
    </div>
</div> 
<script src='/inventori/assets/js/pages/dashboard/index.js'></script>