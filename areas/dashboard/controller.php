<?php
    
    include 'repositories/base_repository.php';
    include 'repositories/barang_masuk_repository.php';
    include 'repositories/barang_keluar_repository.php';

    $title ='Dashboard';
    $connection = mysqli_connect($DbServer, $DbUser, $DbPassword,$DbName);
    
    $total_barang_masuk = 0;
    $total_penjualan = 0;
    $total_pendapatan_perhari = 0;
    $total_pendapatan_perbulan = 0;
    
    $barang_masuk_repository = new barang_masuk_repository();
    $barang_keluar_repository = new barang_keluar_repository();
    
    $total_barang_masuk = $barang_masuk_repository->get_total_barang_masuk();    
    $total_penjualan = $barang_keluar_repository->get_total_penjualan();
    $total_pendapatan_perhari = $barang_keluar_repository->get_total_penjualan_perhari();
    $total_pendapatan_perbulan = $barang_keluar_repository->get_total_penjualan_perbulan();

    include 'view/index.php';
?>