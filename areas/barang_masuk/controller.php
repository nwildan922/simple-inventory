<?php
    include 'repositories/unit_of_work.php';
    
    $barang_masuk_repository = new barang_masuk_repository();
    $supplier_repository = new supplier_repository();

    $title ='Input Barang Masuk';
    $connection = mysqli_connect($DbServer, $DbUser, $DbPassword,$DbName);
    $CurrentUrl = $BaseUrl.'?page=barang';
    // initialize data
    $id = '';
    $tanggal = date('Y-m-d');
    $penerima = '';
    $id_supplier = 0;

    if (isset($_GET['action'])) {
        $action = strtolower($_GET['action']);
        if($action == 'add') {
            $selected_id = 0;
            $resultSupplier = $supplier_repository->get_all();
            include 'view/submit.php';
        }
        
        if($action == 'view') {
            $selected_id = $_GET['id'];
            include 'view/submit.php';
        }

        if($action == 'delete'){
            if(isset($_GET['id'])){
                $id = $_GET['id'];
                $result = $barang_masuk_repository->delete($id);  
                if($result)
                    echo GetMessage('hapus',$CurrentUrl);
                else
                    echo GetErrorMessage();          
            }
        }
    }else{
        $result = $barang_masuk_repository->get_data();       
        include 'view/index.php';
    }
?>