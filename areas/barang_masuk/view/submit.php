<?php include 'popup_barang.php'; ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="card-title">
                <div class="title">Input Data</div>
                </div>
            </div>
            <div class="panel-body">
                <form class="col-md-12" action='#' method='#'>
                    <input type='hidden' id='id' name='id' value='<?php echo $selected_id ?>'/>
                    <div class="form-group">
                        <label>Tanggal</label>
                        <input id='tanggal' type="date" class="form-control" name='tanggal' value='<?php echo $tanggal ?>'>
                    </div>
                    <div class="form-group">
                        <label>Penerima</label>
                        <input id='penerima' type="text" class="form-control" name='penerima' value='<?php echo $penerima ?>'>
                    </div>
                    <div class="form-group">
                        <label>Supplier</label>
                        <select id='id_supplier' class='form-control' name='id_supplier'>
                            <option value="0">Pilih Supplier</option>
                            <?php
                                while($row = mysqli_fetch_array($resultSupplier,MYSQLI_ASSOC)){
                                    echo "<option value=".$row['id'].">".$row['nama_supplier']."</option>";
                                }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <button id='btn_tambah' type="button" class="btn btn-primary btn-sm btn-tambah pull-right" data-toggle="modal" data-target="#myModal">Tambah</button>
                        <input type='hidden' id='tblDataCount' value='0'>
                        <table id='table' class="table table-bordered table-stripped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kode Barang</th>
                                    <th>Nama Barang</th>
                                    <th>Kuantiti</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody id="data_tbl_barang_masuk">
                            </tbody>
                        </table>
                    </div>
                    <button id="btn_submit" type="button" class="btn btn-success" onclick='onSubmit()'>Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
<script src='/inventori/assets/js/pages/barang-masuk/submit.js'></script>