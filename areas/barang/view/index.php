<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="card-title">
                <div class="title">List Data Barang</div>
                </div>
            </div>
            <div class="panel-body">
                <a href='?page=barang&action=add' class='btn btn-primary btn-tambah'>Tambah</a>
                <table id='table' class="table table-bordered table-striped">
                    <thead>
                        <th>No</th>
                        <th>Kode Barang</th>
                        <th>Nama Barang</th>
                        <th>Jenis Barang</th>
                        <th>Harga</th>
                        <th>Aksi</th>
                    </thead>
                    <tbody>
                        <?php
                            $no = 1;
                            while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){
                                $id = $row['id'];
                                $kodebarang = $row['kode_barang'];
                                $namabarang = $row['nama_barang'];
                                $jenisbarang = $row['jenis_barang'];
                                $harga = $row['harga'];
                                echo '<tr>'.
                                        '<td>'.$no.'</td>'.
                                        '<td>'.$kodebarang.'</td>'.
                                        '<td>'.$namabarang.'</td>'.
                                        '<td>'.$jenisbarang.'</td>'.
                                        '<td>'.$harga.'</td>'.
                                        '<td><a href="?page=barang&action=edit&id='.$id.'"class="btn btn-warning btn-sm">Edit</a>|<button class="btn btn-danger btn-sm" onclick="onDelete('.$id.')">Hapus</button></td>'.
                                    '</tr>';
                                $no++;
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script src='/inventori/assets/js/pages/barang/index.js'></script>