<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="card-title">
                <div class="title">Input Data</div>
                </div>
            </div>
            <div class="panel-body">
                <form class="col-md-12" action='?page=barang&action=submit' method='post'>
                    <input type='hidden' name='id' value='<?php echo $id ?>'/>
                    <div class="form-group">
                        <label>Kode Barang</label>
                        <input type="text" class="form-control" name='kode_barang' value='<?php echo $kode_barang ?>' required>
                    </div>
                    <div class="form-group">
                        <label>Nama Barang</label>
                        <input type="text" class="form-control" name='nama_barang' value='<?php echo $nama_barang ?>' required>
                    </div>
                    <div class="form-group">
                        <label>Jenis Barang</label>
                        <input type='hidden' name='selected_jenis_barang' value='<?php echo $jenis_barang ?>' required>
                        <select class="form-control" name='jenis_barang' require>
                            <option value="">-</option>
                            <?php 
                                if($jenis_barang == 'Botol'){
                                    echo '<option value="Botol" selected="selected">Botol</option>';
                                }else{
                                    echo '<option value="Botol">Botol</option>';
                                } 
                                
                                if($jenis_barang == 'Tablet'){
                                    echo '<option value="Tablet" selected="selected">Tablet</option>';
                                }else{
                                    echo '<option value="Tablet">Tablet</option>';
                                } 
                                
                                if($jenis_barang == 'Strip'){
                                    echo '<option value="Strip" selected="selected">Strip</option>';
                                }else{
                                    echo '<option value="Strip">Strip</option>';
                                } 
                                if($jenis_barang == 'Sachet'){
                                    echo '<option value="Sachet" selected="selected">Sachet</option>';
                                }else{
                                    echo '<option value="Sachet">Sachet</option>';
                                } 
                                                               
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Harga</label>
                        <input type="number" class="form-control" name='harga' value='<?php echo $harga ?>' required>
                    </div>
                    <button type="submit" class="btn btn-success">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>