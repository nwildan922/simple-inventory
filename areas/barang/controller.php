<?php
    include 'repositories/unit_of_work.php';    
    $barang_repository = new barang_repository();
    $title ='Master Barang';
    $connection = mysqli_connect($DbServer, $DbUser, $DbPassword,$DbName);
    $CurrentUrl = $BaseUrl.'?page=barang';

    if (isset($_GET['action'])) {
        $action = strtolower($_GET['action']);
        if($action == 'add') {
            $id = '';
            $kode_barang = '';
            $nama_barang = '';
            $jenis_barang = '';
            $harga = '';
            include 'view/submit.php';
        }

        if($action == 'edit') {
            $id = '';
            $kode_barang = '';
            $nama_barang = '';
            if(isset($_GET['id'])){
                $id = $_GET['id'];
                $result = $barang_repository->get_by_id($id);                                        
                $data = mysqli_fetch_array($result,MYSQLI_ASSOC);
                $kode_barang = $data['kode_barang'];
                $nama_barang = $data['nama_barang'];
                $jenis_barang = $data['jenis_barang'];
                $harga = $data['harga'];
            }
            include 'view/submit.php';
        }
        
        if($action == 'submit') {
            $id = '0';
            $kode_barang = '';
            $nama_barang = '';
            if(isset($_POST['id'])){
                $id = $_POST['id'];
                $kode_barang = $_POST['kode_barang'];
                $nama_barang = $_POST['nama_barang'];
                $jenis_barang = $_POST['jenis_barang'];
                $harga = $_POST['harga'];
                if($id == 0){
                    $result = $barang_repository->add($kode_barang,$nama_barang,$jenis_barang,$harga);                                        
                    if($result)
                        echo GetMessage('simpan',$CurrentUrl);
                    else
                        echo GetErrorMessage();   
                } else {
                    $result = $barang_repository->edit($id,$kode_barang,$nama_barang,$jenis_barang,$harga);                    
                    if($result)
                        echo GetMessage('simpan',$CurrentUrl);         
                    else
                        echo GetErrorMessage();
                }
            }
        }

        if($action == 'delete'){
            if(isset($_GET['id'])){
                $id = $_GET['id'];
                $result = $barang_repository->delete($id);
                if($result)
                    echo GetMessage('hapus',$CurrentUrl);
                else
                    echo GetErrorMessage();          
            }
        }
    }else{
        $result = $barang_repository->get_all();
        include 'view/index.php';
    }
?>