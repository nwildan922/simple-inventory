<?php
    if($action == 'add'){     
        $json = file_get_contents('php://input');
        $data = json_decode($json);
        
        $tanggal = $data->tanggal;
        $total = $data->total;
        $bayar = $data->bayar;
        $kembalian = $data->kembalian;
        session_start();
        $id_pengguna = $_SESSION['userid'];
        $items = $data->items;
    
        //query insert header
        $query = 'insert into barang_keluar_header (tanggal,id_pengguna,total,bayar,kembalian) values ("'.$tanggal.'","'.$id_pengguna.'",'.$total.','.$bayar.','.$kembalian.')';
        if (mysqli_query($connection,$query)) {
            $id_header = mysqli_insert_id($connection);                        
            for ($i = 0; $i < count($items); $i++) {
                $id_barang =  $items[$i]->id_barang;
                $kuantiti = $items[$i]->kuantiti;  
                $harga = $items[$i]->harga;  
                //query insert detail  
                $query = 'insert into barang_keluar_detail (id_header,id_barang,kuantiti,harga) values ('.$id_header.','.$id_barang.','.$kuantiti.','.$harga.')';
                mysqli_query($connection,$query); 
                 
                //update stok
                $queryGetKuantiti = 'SELECT id,kuantiti FROM stok_fifo WHERE id_barang='.$id_barang.' AND kuantiti > 0 ORDER BY tanggal_masuk';
                $resultQueryGetKuantiti = mysqli_query($connection,$queryGetKuantiti);
                $processFinish = false;
                while($row = mysqli_fetch_array($resultQueryGetKuantiti,MYSQLI_ASSOC)){
                    if(!$processFinish){
                        $existingKuantiti = $row['kuantiti'];
                        $stok_fifo_id = $row['id'];
                        if($existingKuantiti >= $kuantiti){
                            $processFinish = true;
                            $existingKuantiti -= $kuantiti;    
                            $queryUpdateKuantiti = 'update stok_fifo set kuantiti='.$existingKuantiti.' WHERE id='.$stok_fifo_id;
                            mysqli_query($connection,$queryUpdateKuantiti);      
                        }else{
                            $kuantiti -= $existingKuantiti;    
                            $queryUpdateKuantiti = 'update stok_fifo set kuantiti=0 WHERE id='.$stok_fifo_id;
                            mysqli_query($connection,$queryUpdateKuantiti);
                        }
                    }
                }                  
            }
            $response = GetResponse(true,'Data berhasil disimpan');
            echo json_encode($response);
        } else{
            $response = GetResponse(false,'Terjadi kesalahan harap hubungi administrator');
            echo json_encode($response);
        }
        exit;
    }
    if($action == 'view'){
        $id = 0;
        if(isset($_GET['page'])){
            $id = $_GET['id'];
        }
        $isSuccess = true;
        $message = '';
        $dataHeader = (object) [
            'tanggal' => '',
            'total' => '',
            'bayar' => '',            
            'kembalian' => ''            
        ];
        $listDetail = [];
        $queryHeader = 'select * from barang_keluar_header where id = '.$id;
        $resultQueryHeader = mysqli_query($connection,$queryHeader);
        while($row = mysqli_fetch_array($resultQueryHeader,MYSQLI_ASSOC)){
            $dataHeader->tanggal = $row['tanggal'];
            $dataHeader->total = $row['total'];
            $dataHeader->bayar = $row['bayar'];
            $dataHeader->kembalian = $row['kembalian'];
        }   
        $queryDetail = 'SELECT b.kode_barang, b.nama_barang,d.kuantiti, d.harga,(d.kuantiti * d.harga) as subtotal
                        FROM barang_keluar_detail d
                        JOIN barang b ON d.id_barang = b.id
                        WHERE id_header = '.$id;
        $resultQueryDetail = mysqli_query($connection,$queryDetail);
        while($row = mysqli_fetch_array($resultQueryDetail,MYSQLI_ASSOC)){
            $dataDetail = (object) [
                'kode_barang' => $row['kode_barang'],
                'nama_barang' => $row['nama_barang'],
                'harga' => $row['harga'],
                'kuantiti' => $row['kuantiti'],
                'subtotal' => $row['subtotal']
            ];
            array_push($listDetail,$dataDetail);
        } 
        $data = (object) [
            'header' => $dataHeader,
            'detail' => $listDetail,
        ];
        $response = (object) [
            'data' => $data,
            'isSuccess' => $isSuccess,
            'message' => $message,
        ];
        echo json_encode($response);
    }
    if($action == 'chart'){
        $data = [];
        $isSuccess = true;
        $message = null;
        $current_year = date('yy');
        $current_month = date('m');
        $query = 'SELECT b.jenis_barang, COUNT(b.jenis_barang) AS total
                    FROM barang_keluar_header h 
                    join barang_keluar_detail d on h.id = d.id_header
                    JOIN barang b ON d.id_barang = b.id 
                    WHERE YEAR(h.tanggal) = '.$current_year.' AND MONTH(h.tanggal)= '.$current_month.'
                    GROUP BY b.jenis_barang';
        
        $result = mysqli_query($connection,$query);
        $rowcount = mysqli_num_rows($result);
        if($rowcount > 0){
            while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $temp = (object) [
                    'label' => $row['jenis_barang'],
                    'value' => $row['total']
                ];
                array_push($data,$temp);
            }        
        }
        $response = (object) [
            'data' => $data,
            'isSuccess' => $isSuccess,
            'message' => $message,
        ];
        echo json_encode($response);
    }
    if($action == 'chart-line'){
        $data = [];
        $isSuccess = true;
        $message = null;
        $start_year = date('yy') - 4;
        $end_year = date('yy');
        $current_month = date('m');

        $query = 'SELECT YEAR(h.tanggal) AS tahun, SUM(h.total) AS total
                    FROM barang_keluar_header h 
                    WHERE YEAR(h.tanggal) BETWEEN '.$start_year.' AND '.$end_year.'
                    GROUP BY YEAR(h.tanggal)';
        for($i=5;$i>0;$i--){
            $tempData = (object) [
                'year' => $start_year,
                'value' => 0
            ];
            array_push($data,$tempData);
            $start_year++;
        }
        $result = mysqli_query($connection,$query);
        $rowcount = mysqli_num_rows($result);
        if($rowcount > 0){
            while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){
                for($i=0;$i<5;$i++){
                    if($data[$i]->year == $row['tahun']){
                        $data[$i]->value = $row['total'];
                    }
                }
            }        
        }
        $response = (object) [
            'data' => $data,
            'isSuccess' => $isSuccess,
            'message' => $message,
        ];
        echo json_encode($response);
    }
?>