<?php
    if($action == 'add'){
        $json = file_get_contents('php://input');
        $data = json_decode($json);
        
        $tanggal = $data->tanggal;
        $penerima = $data->penerima;
        $id_supplier = $data->id_supplier;
        $items = $data->items;
    
        //query insert header
        $query = 'insert into barang_masuk_header (tanggal,penerima,id_supplier) values ("'.$tanggal.'","'.$penerima.'","'.$id_supplier.'")';
        if (mysqli_query($connection,$query)) {
            $id_header = mysqli_insert_id($connection);                        
            for ($i = 0; $i < count($items); $i++) {
                $id_barang =  $items[$i]->id_barang;
                $kuantiti = $items[$i]->kuantiti;  
                //query insert detail  
                $query = 'insert into barang_masuk_detail (id_header,id_barang,kuantiti) values ('.$id_header.','.$id_barang.','.$kuantiti.')';
                mysqli_query($connection,$query); 
                //update stok


                //insert stok
                $queryInsertStok = 'insert into stok_fifo (tanggal_masuk,id_barang,kuantiti)values ("'.$tanggal.'",'.$id_barang.','.$kuantiti.')';
                mysqli_query($connection,$queryInsertStok);

                // $queryGetKuantiti = 'SELECT kuantiti FROM stok_fifo WHERE id_barang='.$id_barang;
                // if($resultQueryGetKuantiti = mysqli_query($connection,$queryGetKuantiti)){
                //     $rowcount = mysqli_num_rows($resultQueryGetKuantiti);
                //     if($rowcount > 0){
                //         while($row = mysqli_fetch_array($resultQueryGetKuantiti,MYSQLI_ASSOC)){
                //             $existingKuantiti = $row['kuantiti'];
                //             $existingKuantiti += $kuantiti;
                //             $queryUpdateKuantiti = 'update stok_fifo set kuantiti='.$existingKuantiti.' WHERE id_barang='.$id_barang;
                //             mysqli_query($connection,$queryUpdateKuantiti);  
                //         }        
                //     }else{
                //         //insert stok
                //         $queryInsertStok = 'insert into stok_fifo (tanggal_masuk,id_barang,kuantiti)values ("'.$tanggal.'",'.$id_barang.','.$kuantiti.')';
                //         mysqli_query($connection,$queryInsertStok);
                //     }
                // }
            }
            $response = GetResponse(true,'Data berhasil disimpan');
            echo json_encode($response);
        } else{
            $response = GetResponse(false,'Terjadi kesalahan harap hubungi administrator');
            echo json_encode($response);
        }
        exit;
    }
    if($action == 'view'){
        $id = 0;
        if(isset($_GET['page'])){
            $id = $_GET['id'];
        }
        $isSuccess = true;
        $message = '';
        $dataHeader = (object) [
            'tanggal' => '',
            'penerima' => '',
            'id_supplier' => ''            
        ];
        $listDetail = [];
        $queryHeader = 'select * from barang_masuk_header where id = '.$id;
        $resultQueryHeader = mysqli_query($connection,$queryHeader);
        while($row = mysqli_fetch_array($resultQueryHeader,MYSQLI_ASSOC)){
            $dataHeader->tanggal = $row['tanggal'];
            $dataHeader->penerima = $row['penerima'];
            $dataHeader->id_supplier = $row['id_supplier'];
        }   
        $queryDetail = 'SELECT b.kode_barang, b.nama_barang,d.kuantiti
                        FROM barang_masuk_detail d
                        JOIN barang b ON d.id_barang = b.id
                        WHERE id_header = '.$id;
        $resultQueryDetail = mysqli_query($connection,$queryDetail);
        while($row = mysqli_fetch_array($resultQueryDetail,MYSQLI_ASSOC)){
            $dataDetail = (object) [
                'kode_barang' => $row['kode_barang'],
                'nama_barang' => $row['nama_barang'],
                'kuantiti' => $row['kuantiti']
            ];
            array_push($listDetail,$dataDetail);
        } 
        $data = (object) [
            'header' => $dataHeader,
            'detail' => $listDetail,
        ];
        $response = (object) [
            'data' => $data,
            'isSuccess' => $isSuccess,
            'message' => $message,
        ];
        echo json_encode($response);
    }
?>