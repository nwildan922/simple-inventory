<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="card-title">
                <div class="title">Filter</div>
                </div>
            </div>
            <div class="panel-body">
                <form method='get' action='#'>
                    <div class="form-group col-md-6">
                        <label>Tanggal Mulai</label>
                        <input id='start_date' type='date' class='form-control' value='<?php echo $start_date ?>'/>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Tanggal Akhir</label>
                        <input id='end_date' type='date' class='form-control' value='<?php echo $end_date ?>'/>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Penerima</label>
                        <input id='penerima' type='text' class='form-control' value='<?php echo $penerima ?>'/>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Supplier</label>
                        <select id='id_supplier' class='form-control' value='<?php echo $id_supplier ?>'>
                            <option value='-'>-</option>
                            <?php
                                while($row = mysqli_fetch_array($result_supplier,MYSQLI_ASSOC)){
                                    if($id_supplier != $row['id']){
                                        echo "<option value=".$row['id'].">".$row['nama_supplier']."</option>";
                                    }else{
                                        echo "<option value=".$row['id']." selected='selected'>".$row['nama_supplier']."</option>";
                                    }
                                }
                            ?>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <button class='btn btn-primary' type='button' onclick='onSearch()'>Cari</button>
                    </div>
                </form>

                <table id='table' class="table table-bordered table-striped">
                    <thead>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Penerima</th>
                        <th>Supplier</th>
                        <th>Total Barang Masuk</th>
                    </thead>
                    <tbody>
                        <?php
                            $no = 1;
                            while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){
                                $id = $row['id'];
                                $tanggal = $row['tanggal'];
                                $penerima = $row['penerima'];
                                $nama_supplier = $row['nama_supplier'];
                                $total_barang_masuk = $row['total_barang_masuk'];
                                echo '<tr>'.
                                        '<td>'.$no.'</td>'.
                                        '<td>'.$tanggal.'</td>'.
                                        '<td>'.$penerima.'</td>'.
                                        '<td>'.$nama_supplier.'</td>'.
                                        '<td>'.$total_barang_masuk.'</td>'.
                                    '</tr>';
                                $no++;
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    function onSearch(){
        const start_date = $('#start_date').val();
        const end_date = $('#end_date').val();
        const penerima = $('#penerima').val();
        const id_supplier = $('#id_supplier').val();
        let url = `http://localhost/inventori?page=laporan-barang-masuk&action=search&start_date=${start_date}&end_date=${end_date}&id_supplier=${id_supplier}&penerima=${penerima}`;
        window.location.href = url;
    }
</script>