<?php
    include 'repositories/unit_of_work.php'; 
    
    if(!isset($_GET['start_date'])){
        $start_date = date('yy-m-d');
    }else{
        $start_date = $_GET['start_date'];
    }

    if(!isset($_GET['end_date'])){
        $end_date = date('yy-m-d');
    }else{
        $end_date = $_GET['end_date'];
    }

    if(!isset($_GET['penerima'])){
        $penerima = '';
    }else{
        $penerima = $_GET['penerima'];
    }

    if(!isset($_GET['id_supplier'])){
        $id_supplier = '-';
    }else{
        $id_supplier = $_GET['id_supplier'];
    }
    
    $barang_masuk_repository = new barang_masuk_repository();
    $supplier_repository = new supplier_repository();
    $result = $barang_masuk_repository->get_data_filter($start_date,$end_date, $penerima, $id_supplier);
    $result_supplier = $supplier_repository->get_all();
    include 'view/index.php';
?>