<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="card-title">
                <div class="title">Input Data</div>
                </div>
            </div>
            <div class="panel-body">
                <form class="col-md-12" action='?page=<?php echo $currentPage ?>&action=submit' method='post'>
                    <input type='hidden' name='id' value='<?php echo $id ?>'/>
                    <div class="form-group">
                        <label>Username</label>
                        <input type="text" class="form-control" name='username' value='<?php echo $username ?>' required>
                    </div>
                    <div class="form-group">
                        <label>Nama Lengkap </label>
                        <input type="text" class="form-control" name='nama_lengkap' value='<?php echo $nama_lengkap ?>' required>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" name='password' value='<?php echo $password ?>' required>
                    </div>
                    <div class="form-group">
                        <label>No HP</label>
                        <input type="text" class="form-control" name='no_hp' value='<?php echo $no_hp ?>' required>
                    </div>
                    <button type="submit" class="btn btn-success">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>