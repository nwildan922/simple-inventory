<?php
    include 'repositories/unit_of_work.php';

    $pengguna_repository = new pengguna_repository();
    $title ='Master Pengguna';
    $connection = mysqli_connect($DbServer, $DbUser, $DbPassword,$DbName);
    $currentPage = 'pengguna';
    $CurrentUrl = $BaseUrl.'?page='.$currentPage;

    if (isset($_GET['action'])) {
        $action = strtolower($_GET['action']);
        // initialize model
        $id = '0';
        $username = '';
        $password = '';
        $nama_lengkap = '';
        $no_hp = '';
        
        if($action == 'add') {
            include 'view/submit.php';
        }

        if($action == 'edit') {
            if(isset($_GET['id'])){
                $id = $_GET['id'];
                $result = $pengguna_repository->get_by_id($id);
                $data = mysqli_fetch_array($result,MYSQLI_ASSOC);
                $username = $data['username'];
                $nama_lengkap = $data['nama_lengkap'];
                $no_hp = $data['no_hp'];
            }
            include 'view/submit.php';
        }
        
        if($action == 'submit') {

            if(isset($_POST['id'])){
                $id = $_POST['id'];
                $username = $_POST['username'];
                $nama_lengkap = $_POST['nama_lengkap'];
                $password = $_POST['password'];
                $no_hp = $_POST['no_hp'];
                $encryptedPassword = md5($password);
                if($id == 0){
                    
                    $result = $pengguna_repository->add($username,$nama_lengkap,$encryptedPassword,$no_hp);
                    if($result)
                        echo GetMessage('simpan',$CurrentUrl);
                    else
                        echo GetErrorMessage();   
                }else{
                    $result = $pengguna_repository->edit($id,$username,$nama_lengkap,$encryptedPassword,$no_hp);
                    if($result)
                        echo GetMessage('simpan',$CurrentUrl);         
                    else
                        echo GetErrorMessage();
                }
            }
        }

        if($action == 'delete'){
            if(isset($_GET['id'])){
                $id = $_GET['id'];
                $result = $pengguna_repository->delete($id);
                if($result)
                    echo GetMessage('hapus',$CurrentUrl);
                else
                    echo GetErrorMessage();          
            }
        }
    }else{
        $result = $pengguna_repository->get_all();
        include 'view/index.php';
    }
?>