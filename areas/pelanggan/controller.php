<?php
    $title ='Master Pelanggan';
    $connection = mysqli_connect($DbServer, $DbUser, $DbPassword,$DbName);
    $currentPage = 'pelanggan';
    $CurrentUrl = $BaseUrl.'?page='.$currentPage;

    if (isset($_GET['action'])) {
        $action = strtolower($_GET['action']);
        // initialize model
        $id = '0';
        $nama = '';
        $alamat = '';
        $no_hp = '';
        
        if($action == 'add') {
            include 'view/submit.php';
        }

        if($action == 'edit') {
            if(isset($_GET['id'])){
                $id = $_GET['id'];
                $query = "select * from pelanggan where id = '$id'";
                $result = mysqli_query($connection,$query);
                $data = mysqli_fetch_array($result,MYSQLI_ASSOC);
                $nama = $data['nama_pelanggan'];
                $alamat = $data['alamat'];
                $no_hp = $data['no_hp'];
            }
            include 'view/submit.php';
        }
        
        if($action == 'submit') {

            if(isset($_POST['id'])){
                $id = $_POST['id'];
                $nama = $_POST['nama'];
                $alamat = $_POST['alamat'];
                $no_hp = $_POST['no_hp'];

                if($id == 0){
                    $query = "insert into pelanggan (nama_pelanggan,alamat,no_hp) values ('$nama','$alamat',$no_hp)";
                    $result = mysqli_query($connection,$query);
                    if($result)
                        echo GetMessage('simpan',$CurrentUrl);
                    else
                        echo GetErrorMessage();   
                }else{
                    $query = "update pelanggan set nama_pelanggan='$nama',alamat='$alamat',no_hp='$no_hp' where id = '$id'";       
                    $result = mysqli_query($connection,$query);
                    if($result)
                        echo GetMessage('simpan',$CurrentUrl);         
                    else
                        echo GetErrorMessage();
                }
            }
        }

        if($action == 'delete'){
            if(isset($_GET['id'])){
                $id = $_GET['id'];
                $query = "delete from pelanggan where id = '$id'";
                $result = mysqli_query($connection,$query);
                if($result)
                    echo GetMessage('hapus',$CurrentUrl);
                else
                    echo GetErrorMessage();          
            }
        }
    }else{
        $query = "select * from pelanggan";
        $result = mysqli_query($connection,$query);        
        include 'view/index.php';
    }
?>