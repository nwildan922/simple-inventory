<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="card-title">
                <div class="title">List Data Pelanggan</div>
                </div>
            </div>
            <div class="panel-body">
                <a href='?page=<?php echo $currentPage ?>&action=add' class='btn btn-primary btn-tambah'>Tambah</a>
                <table id='table' class="table table-bordered table-striped">
                    <thead>
                        <th>No</th>
                        <th>Nama Pelanggan</th>
                        <th>Alamat</th>
                        <th>No Handphone</th>
                        <th>Aksi</th>
                    </thead>
                    <tbody>
                        <?php
                            $no = 1;
                            while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){
                                $id = $row['id'];
                                $nama = $row['nama_pelanggan'];
                                $alamat = $row['alamat'];
                                $no_hp = $row['no_hp'];
                                echo '<tr>'.
                                        '<td>'.$no.'</td>'.
                                        '<td>'.$nama.'</td>'.
                                        '<td>'.$alamat.'</td>'.
                                        '<td>'.$no_hp.'</td>'.
                                        '<td><a href="?page='.$currentPage.'&action=edit&id='.$id.'"class="btn btn-warning btn-sm">Edit</a>|<button class="btn btn-danger btn-sm" onclick="onDelete('.$id.')">Hapus</button></td>'.
                                    '</tr>';
                                $no++;
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>

    function onDelete(id){
        var result = confirm('Apakah anda yakin akan menghapus data ?');
        if(result){
            window.location.href = `?page=<?php echo $currentPage ?>&action=delete&id=${id}`;
        }else{
            alert('Proses hapus digagalkan');
        }
    }
</script>