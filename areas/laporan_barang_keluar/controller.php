<?php
    include 'repositories/unit_of_work.php'; 
    
    if(!isset($_GET['start_date'])){
        $start_date = date('yy-m-d');
    }else{
        $start_date = $_GET['start_date'];
    }

    if(!isset($_GET['end_date'])){
        $end_date = date('yy-m-d');
    }else{
        $end_date = $_GET['end_date'];
    }

    if(!isset($_GET['id_petugas'])){
        $id_petugas = '';
    }else{
        $id_petugas = $_GET['id_petugas'];
    }
    
    $barang_keluar_repository = new barang_keluar_repository();
    $pengguna_repository = new pengguna_repository();
    $result = $barang_keluar_repository->get_data_filter($start_date,$end_date, $id_petugas);
    $result_pengguna = $pengguna_repository->get_all();
    include 'view/index.php';
?>