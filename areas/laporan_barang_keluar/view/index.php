<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="card-title">
                <div class="title">Filter</div>
                </div>
            </div>
            <div class="panel-body">
                <form method='get' action='#'>
                    <div class='row'>
                        <div class="form-group col-md-6">
                            <label>Tanggal Mulai</label>
                            <input id='start_date' type='date' class='form-control' value='<?php echo $start_date ?>'/>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Tanggal Akhir</label>
                            <input id='end_date' type='date' class='form-control' value='<?php echo $end_date ?>'/>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="form-group col-md-6">
                            <label>Petugas</label>
                            <select id='id_petugas' class='form-control' value='<?php echo $id_petugas ?>'>
                                <option value='-'>-</option>
                                <?php
                                    while($row = mysqli_fetch_array($result_pengguna,MYSQLI_ASSOC)){
                                        if($id_petugas != $row['id']){
                                            echo "<option value=".$row['id'].">".$row['nama_lengkap']."</option>";
                                        }else{
                                            echo "<option value=".$row['id']." selected='selected'>".$row['nama_lengkap']."</option>";
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>                    
                    <div class='row'>
                        <div class="form-group col-md-6">
                            <button class='btn btn-primary' type='button' onclick='onSearch()'>Cari</button>
                        </div>
                    </div>

                </form>

                <table id='table' class="table table-bordered table-striped">
                    <thead>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Petugas</th>
                        <th>Total Barang Keluar</th>
                    </thead>
                    <tbody>
                        <?php
                            $no = 1;
                            while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){
                                $id = $row['id'];
                                $tanggal = $row['tanggal'];
                                $petugas = $row['nama_lengkap'];
                                $total_barang_keluar = $row['total_barang_masuk'];
                                echo '<tr>'.
                                        '<td>'.$no.'</td>'.
                                        '<td>'.$tanggal.'</td>'.
                                        '<td>'.$petugas.'</td>'.
                                        '<td>'.$total_barang_keluar.'</td>'.
                                    '</tr>';
                                $no++;
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    function onSearch(){
        const start_date = $('#start_date').val();
        const end_date = $('#end_date').val();
        const id_petugas = $('#id_petugas').val();
        let url = `http://localhost/inventori?page=laporan-barang-keluar&action=search&start_date=${start_date}&end_date=${end_date}&id_petugas=${id_petugas}`;
        window.location.href = url;
    }
</script>