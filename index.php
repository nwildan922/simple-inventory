﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Inventori App</title>
        <?php include 'shared/content.php'; ?>
    </head>
    <body>
        <div id="wrapper">

            <?php 
                session_start();
                $is_login = isset($_SESSION['userid']) && !empty($_SESSION['userid']);
                if($is_login){
                    include 'main.php';
                }else{
                    //redirect to login page
                    echo "<script>window.location.href = 'http://localhost/inventori/authentication/login.php'</script>";
                }
            ?>
        </div>
    </body>
</html>