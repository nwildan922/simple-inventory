
<link href="https://fonts.googleapis.com/css?family=Fredoka+One" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
<link rel="stylesheet" href="/inventori/assets/css/error-page-style.css"> 
<div id="notfound">
    <div class="notfound">
        <div class="notfound-404">
            <h1>403</h1>
        </div>
        <h2>Oops, it seems that you not login yet</h2>
        <a href="/inventori/authentication/login.php"><span class="arrow"></span>Click here to login</a>
    </div>
</div>
<script>
    onInit();
    function onInit(){
        window.location.href = "http://localhost/inventori/authentication/login.php";
    }
</script>