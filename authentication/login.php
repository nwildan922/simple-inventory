<?php include 'auth.php'; ?>
<link href="../assets/css/bootstrap.css" rel="stylesheet" />
<link href="../assets/css/login.css" rel="stylesheet" />
<div class="sidenav">
    <div class="login-main-text">
    <img class='logo-image' src="../assets/images/drug.png">
    <h2><i class="icon fa fa-medkit"></i> Klinik DKT Pangrango <br> Halaman Login</h2>
    <p>Selamat Datang.</p>
    </div>
</div>
<div class="main">

    <div class="col-md-6 col-sm-12">
    <div class="login-form">
        <form action='login.php' method='post'>
            <div class="form-group">
                <label>User Name</label>
                <input type="text" class="form-control" name='username' placeholder="User Name">
            </div>
            <div class="form-group">
                <label>Password</label>
                <input type="password" class="form-control" name='password' placeholder="Password">
            </div>
            <?php 
                if($isLoginFailed){
                    echo "<div class='alert alert-danger'>Username atau password salah</div>";
                }            
            ?>
            <button type="submit" class="btn btn-black">Login</button>
            <button type="reset" class="btn btn-secondary">Batal</button>
        </form>
    </div>
    </div>
</div>