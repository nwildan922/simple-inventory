<?php
    include 'shared/header.php';
    include 'shared/navigation.php'; 
?>
<div id="page-wrapper">
    <div class="header">
        <h1 class="page-header"><?php echo $title ?></h1>                    
    </div>
    <div id="page-inner">              
        <?php 
            include $Controller;
        ?>      
    </div>
</div>