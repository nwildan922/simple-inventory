<nav class="navbar-default navbar-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="main-menu">
            <li>
                <a class="<?php echo IsActiveMenu('dashboard') ?>" href="<?php echo $BaseUrl; ?>"><i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            <li class='<?php echo IsActiveParentMenu('master') ?>'>
                <a href="#"><i class="fa fa-database"></i> Master Data<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a class='<?php echo IsActiveMenu('barang') ?>' href="<?php echo $BaseUrl; ?>?page=barang">Barang</a>
                    </li>
                    <li>
                        <a class='<?php echo IsActiveMenu('supplier') ?>'  href="<?php echo $BaseUrl; ?>?page=supplier">Supplier</a>
                    </li>
                    <li>
                        <a class='<?php echo IsActiveMenu('pengguna') ?>'  href="<?php echo $BaseUrl; ?>?page=pengguna">Pengguna</a>
                    </li>
                </ul>
            </li>							
            <li class='<?php echo IsActiveParentMenu('transaksi') ?>'>
                <a href="#"><i class="fa fa-book"></i> Transaksi<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a class='<?php echo IsActiveMenu('barang-masuk') ?>'  href="<?php echo $BaseUrl; ?>?page=barang-masuk">Barang Masuk</a>
                    </li>
                    <li>
                        <a class='<?php echo IsActiveMenu('barang-keluar') ?>'  href="<?php echo $BaseUrl; ?>?page=barang-keluar">Penjualan</a>
                    </li>
                    <li>
                        <a class='<?php echo IsActiveMenu('stok-barang') ?>'  href="<?php echo $BaseUrl; ?>?page=stok-barang">Stok Barang</a>
                    </li>
                </ul>
            </li>
            <li class='<?php echo IsActiveParentMenu('laporan') ?>'>
                <a href="#"><i class="fa fa-list"></i> Laporan<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a class='<?php echo IsActiveMenu('laporan-barang-masuk') ?>'  href="<?php echo $BaseUrl; ?>?page=laporan-barang-masuk">Laporan Barang Masuk</a>
                    </li>
                    <li>
                        <a class='<?php echo IsActiveMenu('laporan-barang-keluar') ?>'  href="<?php echo $BaseUrl; ?>?page=laporan-barang-keluar">Laporan Barang Keluar</a>
                    </li>
                </ul>
            </li>                  
        </ul>
    </div>
</nav>