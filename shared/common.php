<?php 
    $Environment = 'PHPOnly'; // set environment to PHPOnly or XAMPP
    $BaseUrl = 'http://localhost/inventori';
    $LoginUrl = $BaseUrl.'/authentication/login.php';
    $DbServer = '';
    $DbUser = '';
    $DbPassword = '';
    $DbName = '';
    
    if($Environment == 'PHPOnly'){
        $DbServer = '202.157.185.149:6603';
        $DbUser = 'sa';
        $DbPassword = 'P@ssw0rd';
        $DbName = 'inventori';
    }else{
        $DbServer = 'localhost';
        $DbUser = 'root';
        $DbPassword = '';
        $DbName = 'inventori';
    }
    $ApplicationTitle = 'Inventori';

    function GetMessage($action,$link){
        return '<div class="alert alert-success">
                    <h3>Data berhasil di '.$action.'</h3><br/><br/>halaman akan segera dialihkan,atau klik <a href="'.$link.'">disini</a> untuk ke halaman list data
                </div>
                <script> setTimeout(() => { window.location.href="'.$link.'";}, 2000); </script>                
                ';
    }

    function GetErrorMessage(){
        return '<div class="alert alert-danger">Terjadi kesalahan harap hubungi administrator</div>';
    }

    function IsActiveMenu($menu){
        $status = '';
        if (isset($_GET['page'])) {
            $currentPage = strtolower($_GET['page']);
            if($currentPage == $menu)
                $status = 'active-menu';
        }else{
            if($menu === 'dashboard')
                $status = 'active-menu';
        }
        return $status;
    }

    function IsActiveParentMenu($menu) {
        $status = '';
        if (isset($_GET['page'])) {
            $currentPage = strtolower($_GET['page']);
            if($menu == 'master'){
                if($currentPage == 'barang' || $currentPage == 'supplier' || $currentPage == 'pelanggan' || $currentPage == 'pengguna')
                    $status = 'active';
            }
            if($menu == 'transaksi'){
                if($currentPage == 'barang-masuk' || $currentPage == 'barang-keluar' || $currentPage == 'stok-barang')
                    $status = 'active';
            }
            if($menu == 'laporan'){
                if($currentPage == 'laporan-barang-masuk' || $currentPage == 'laporan-barang-keluar')
                    $status = 'active';
            }

        }
        return $status;
    }

    function GetResponse($isSuccess, $message){
        $object = (object) [
            'isSuccess' => $isSuccess,
            'message' => $message,
        ];
        return $object;
    }
?>