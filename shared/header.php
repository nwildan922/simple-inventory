<?php 
    $path = $_SERVER['DOCUMENT_ROOT'];
    $path .= "/inventori/shared/common.php";
    include_once($path);
    $currentUser = '';
    if(isset($_SESSION['fullName'])){
        $currentUser = $_SESSION['fullName'];
    }

    $title = 'Dashboard';
    $Controller = 'areas/dashboard/controller.php';

    if (isset($_GET['page'])) {
        $currentPage = strtolower($_GET['page']);
        if($currentPage == 'barang') {
            $title = 'Master Barang';
            $Controller = 'areas/barang/controller.php';
        }
        if($currentPage == 'pelanggan') {
            $title = 'Master Pelanggan';
            $Controller = 'areas/pelanggan/controller.php';
        }
        if($currentPage == 'supplier') {
            $title = 'Master Supplier';
            $Controller = 'areas/supplier/controller.php';
        }
        if($currentPage == 'pengguna') {
            $title = 'Master Pengguna';
            $Controller = 'areas/pengguna/controller.php';
        }
        if($currentPage == 'barang-masuk') {
            $title = 'Transaksi Barang Masuk';
            $Controller = 'areas/barang_masuk/controller.php';
        }
        if($currentPage == 'barang-keluar') {
            $title = 'Transaksi Penjualan';
            $Controller = 'areas/barang_keluar/controller.php';
        }
        if($currentPage == 'stok-barang') {
            $title = 'Stok Barang';
            $Controller = 'areas/stok/controller.php';
        }
        if($currentPage == 'laporan-barang-masuk') {
            $title = 'Laporan Barang Masuk';
            $Controller = 'areas/laporan_barang_masuk/controller.php';
        }
        if($currentPage == 'laporan-barang-keluar') {
            $title = 'Laporan Penjualan';
            $Controller = 'areas/laporan_barang_keluar/controller.php';
        }
    }
?>
<nav class="navbar navbar-default top-navbar" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?php echo $BaseUrl; ?>" style="font-size:16px;"><strong><i class="icon fa fa-medkit"></i> KLINIK DKT PANGRANGO </strong></a>				
        <div id="sideNav" href="">
        <i class="fa fa-bars icon"></i> 
        </div>
    </div>
    <div class='current-user'>
        Welcome : <?php echo $currentUser ?>
    </div> 
    <div class='btn-logout'>
        <a href="<?php echo $BaseUrl; ?>/authentication/logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
    </div>
</nav>