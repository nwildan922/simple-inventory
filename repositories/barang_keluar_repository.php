<?php
    class barang_keluar_repository extends base_repository {
        function __construct() {
            parent::__construct();
        }
        function get_total_penjualan() {
            $current_date = date('yy-m-d');
            $total_penjualan = 0;
            $queryPenjualan = "SELECT h.tanggal,SUM(d.kuantiti) AS total_penjualan
                                FROM barang_keluar_header h
                                JOIN barang_keluar_detail d ON h.id = d.id_header
                                WHERE h.tanggal = '".$current_date."'
                                GROUP BY h.tanggal ";
            $resultPenjualan = mysqli_query(self::$mysqlconnection,$queryPenjualan);
            $rowcountPenjualan = mysqli_num_rows($resultPenjualan);
            if($rowcountPenjualan > 0){
                while($row = mysqli_fetch_array($resultPenjualan,MYSQLI_ASSOC)){
                    $total_penjualan = $row['total_penjualan'];
                }        
            }
            return $total_penjualan;
        }
        function get_total_penjualan_perhari(){
            $total_pendapatan_hari_ini = 0;
            $current_date = date('yy-m-d');
            $queryPendapatan = "SELECT SUM(total) AS total_pendapatan_hari_ini
                        FROM barang_keluar_header
                        WHERE tanggal = '".$current_date."'";
            $resultPendapatan = mysqli_query(self::$mysqlconnection,$queryPendapatan);
            $rowcountPendapatan = mysqli_num_rows($resultPendapatan);
            if($rowcountPendapatan > 0){
                while($row = mysqli_fetch_array($resultPendapatan,MYSQLI_ASSOC)){
                    if($row['total_pendapatan_hari_ini'] != null){
                        $total_pendapatan_hari_ini = $row['total_pendapatan_hari_ini'];
                    }
                }        
            }
            return $total_pendapatan_hari_ini;
        }
        function get_total_penjualan_perbulan(){

            $total_pendapatan_bulan_ini = 0;
            $current_month = date('m');
            $current_year = date('yy');
            $queryPendapatanBulanan = "SELECT SUM(total) AS total_pendapatan_bulan_ini
                                        FROM barang_keluar_header
                                        WHERE year(tanggal) = ".$current_year." AND MONTH(tanggal) = ".$current_month;
            $resultPendapatanBulanan = mysqli_query(self::$mysqlconnection,$queryPendapatanBulanan);
            $rowcountPendapatanBulanan = mysqli_num_rows($resultPendapatanBulanan);
            if($rowcountPendapatanBulanan > 0){
                while($row = mysqli_fetch_array($resultPendapatanBulanan,MYSQLI_ASSOC)){
                    $total_pendapatan_bulan_ini = $row['total_pendapatan_bulan_ini'];
                }        
            }
            return $total_pendapatan_bulan_ini;
        }
        function delete($id){
            $query = "delete from barang_keluar_detail WHERE id IN (
                            SELECT * FROM (
                                SELECT d.id
                                FROM barang_keluar_header h
                                JOIN barang_keluar_detail d ON h.id = d.id_header
                                WHERE h.id = ".$id."
                            ) AS p
                        )";
            $result = mysqli_query(self::$mysqlconnection,$query);
            $query = "DELETE from barang_keluar_header WHERE id = ".$id;
            $result = mysqli_query(self::$mysqlconnection,$query);
            return $result;
        }
        function get_data(){   
            $query = "SELECT h.id, h.tanggal,p.nama_lengkap, SUM(d.kuantiti) AS total_barang_keluar,h.total as total_nominal
                    FROM barang_keluar_header h
                    JOIN barang_keluar_detail d ON h.id = d.id_header
                    JOIN pengguna p ON h.id_pengguna = p.id
                    GROUP BY h.id, h.tanggal ,p.nama_lengkap
                    ORDER BY h.id DESC";
            $result = mysqli_query(self::$mysqlconnection,$query);  
            return $result;
        }

        function get_data_filter($start_date, $end_date,$id_pengguna){
            $query = "SELECT h.id, h.tanggal,p.nama_lengkap, SUM(d.kuantiti) AS total_barang_masuk
                        FROM barang_keluar_header h
                        JOIN barang_keluar_detail d ON h.id = d.id_header
                        JOIN pengguna p ON p.id = h.id_pengguna
                        WHERE
                        ((h.tanggal BETWEEN '".$start_date."' AND '".$end_date."') OR ('".$start_date."'='') OR ('".$end_date."'=''))
                        AND (p.id = '".$id_pengguna."' OR '".$id_pengguna."'='-')
                        GROUP BY h.id, h.tanggal ,p.nama_lengkap
                        ORDER BY h.id DESC";
            $result = mysqli_query(self::$mysqlconnection,$query);  
            return $result;
        }

    }
?>