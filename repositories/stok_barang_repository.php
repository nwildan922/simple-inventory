<?php
    class stok_barang_repository extends base_repository {
        function __construct() {
            parent::__construct();
        }        
        function get_data(){
            $query = "SELECT b.kode_barang, nama_barang, sum(kuantiti) AS kuantiti
                        FROM stok_fifo s  
                        JOIN barang b ON b.id = s.id_barang
                        GROUP BY b.kode_barang, nama_barang;";
            $result = mysqli_query(self::$mysqlconnection,$query);  
            return $result;
        }
    }
?>