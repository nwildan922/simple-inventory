<?php
    class barang_masuk_repository extends base_repository {
        function __construct() {
            parent::__construct();
        }
        function get_total_barang_masuk() {
            $current_date = date('yy-m-d');
            $total_barang_masuk = 0;
            $queryBarangMasuk = "SELECT h.tanggal,SUM(d.kuantiti) AS total_barang_masuk
            FROM barang_masuk_header h
            JOIN barang_masuk_detail d ON h.id = d.id_header
            WHERE h.tanggal = '".$current_date."'
            GROUP BY h.tanggal";

            $resultBarangMasuk = mysqli_query(self::$mysqlconnection,$queryBarangMasuk);
            $rowcountBarangMasuk = mysqli_num_rows($resultBarangMasuk);
            if($rowcountBarangMasuk > 0){
                while($row = mysqli_fetch_array($resultBarangMasuk,MYSQLI_ASSOC)){
                    $total_barang_masuk = $row['total_barang_masuk'];
                }    
            }
            return $total_barang_masuk;
        }
        function delete($id){
            $query = "delete from barang_masuk_detail WHERE id IN (
                        SELECT * FROM (
                            SELECT d.id
                            FROM barang_masuk_header h
                            JOIN barang_masuk_detail d ON h.id = d.id_header
                            WHERE h.id = ".$id."
                        ) AS p
                    )";
            $result = mysqli_query(self::$mysqlconnection,$query);
            $query = "DELETE from barang_masuk_detail WHERE id =".$id;
            $result = mysqli_query(self::$mysqlconnection,$query);
            return $result;
        }
        function get_data(){
            $query = "SELECT h.id, h.tanggal, h.penerima,s.nama_supplier, SUM(d.kuantiti) AS total_barang_masuk
                    FROM barang_masuk_header h
                    JOIN barang_masuk_detail d ON h.id = d.id_header
                    JOIN supplier s ON s.id = h.id_supplier
                    GROUP BY h.id, h.tanggal, h.penerima ,s.nama_supplier
                    ORDER BY h.id DESC";
            $result = mysqli_query(self::$mysqlconnection,$query);  
            return $result;
        }
        function get_data_filter($start_date, $end_date,$penerima,$id_supplier){
            $query = "SELECT h.id, h.tanggal, h.penerima,s.nama_supplier, SUM(d.kuantiti) AS total_barang_masuk
                    FROM barang_masuk_header h
                    JOIN barang_masuk_detail d ON h.id = d.id_header
                    JOIN supplier s ON s.id = h.id_supplier
                    WHERE
                        ((h.tanggal BETWEEN '".$start_date."' AND '".$end_date."') OR ('".$start_date."'='') OR ('".$end_date."'=''))
                        AND (h.id_supplier = '".$id_supplier."' OR '".$id_supplier."'='-')
                        AND (h.penerima LIKE '%".$penerima."%' OR '".$penerima."'='')
                    GROUP BY h.id, h.tanggal, h.penerima ,s.nama_supplier
                    ORDER BY h.id DESC";
            $result = mysqli_query(self::$mysqlconnection,$query);  
            return $result;
        }
    }
?>