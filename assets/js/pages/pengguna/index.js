function onDelete(id){
    var result = confirm('Apakah anda yakin akan menghapus data ?');
    if(result){
        window.location.href = `/inventori?page=pengguna&action=delete&id=${id}`;
    }else{
        alert('Proses hapus digagalkan');
    }
}