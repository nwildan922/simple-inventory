onInit();

function onInit(){

    const id = $('#id').val();
    if(Number(id) > 0){
        enableViewOnly();
        getData();
    }else{
        bindData();
        preventDefaultOnEnter();
    }
}

function enableViewOnly(){
    $('#btn_tambah').hide();
    $('#btn_submit').hide();
}
function getData(){
    debugger;
    const id = $('#id').val();
    $.ajax({
        url: `/inventori/areas/api/rest.php?page=barang-masuk&action=view&id=${id}`,
        type: 'get',
        dataType: 'json',
        contentType: 'application/json',
        success: function (data) {
            if(data.isSuccess){
                bindDataViewOnly(data.data);
            }
        }
    });
}
function preventDefaultOnEnter(){
    $(window).keydown(function(event){
        if(event.keyCode == 13) {
        event.preventDefault();
        return false;
        }
    });

}

function bindData(){
    var data = sessionStorage.getItem('barang-masuk');
    if(data === undefined || data === null || data === "" || data === "[]" ) {
        addEmptyData();                        
    }else{
        var model = JSON.parse(data);
        $('#data_tbl_barang_masuk').empty();
        for(var i=0;i<model.length;i++){
            const id = model[i].id;
            const kode_barang = model[i].kode_barang;
            const nama_barang = model[i].nama_barang;
            const kuantiti = model[i].kuantiti;
            $('#data_tbl_barang_masuk').append(
            `<tr>
                <td>${i + 1}<input type='hidden' value='${id}' /></td>
                <td>${kode_barang}</td>
                <td>${nama_barang}</td>
                <td><input type="number" style='width:50px' value='${kuantiti}' onchange='onUpdateKuantiti(${i},this)' /></td>
                <td>
                    <button type='button' class="btn btn-sm btn-danger" onclick='onRemoveData(${i})'>Hapus</button>
                </td>
            </tr>`
            );
        }
    }
}

function bindDataViewOnly(model) {
    const header = model.header;
    const detail = model.detail;
    $('#data_tbl_barang_masuk').empty();
        for(var i=0;i<detail.length;i++){
            const kode_barang = detail[i].kode_barang;
            const nama_barang = detail[i].nama_barang;
            const kuantiti = detail[i].kuantiti;
            $('#data_tbl_barang_masuk').append(
            `<tr>
                <td>${i + 1}</td>
                <td>${kode_barang}</td>
                <td>${nama_barang}</td>
                <td>${kuantiti}</td>
                <td>-</td>
            </tr>`
            );
        }
}

function addEmptyData (){
    $('#data_tbl_barang_masuk').empty();
    $('#data_tbl_barang_masuk').append(`<tr><td colspan='5' style='text-align:center'>Tidak ada data</td></tr>`);
}

function onRemoveData(index){
    debugger;

    let result = confirm('Apakah anda yakin akan menghapus data ?');

    if(result){
        var data = sessionStorage.getItem('barang-masuk');
        if(data === "" || data === null || data === undefined){
            addEmptyData();
        }else{
            var model = JSON.parse(data);
            model.splice(index,1);
            let modelString = JSON.stringify(model);
            sessionStorage.setItem('barang-masuk',modelString);
            bindData();
            alert('Data berhasil dihapus');
        }
    }else{
        alert('Proses hapus data digagalkan');
    }
}

function onUpdateKuantiti(index,param){
    debugger;
    var data = sessionStorage.getItem('barang-masuk');
    var model = JSON.parse(data);
    model[index].kuantiti = param.value;
    let modelString = JSON.stringify(model);
    sessionStorage.setItem('barang-masuk',modelString);
}

function getSubmittedItem(){
    var data = sessionStorage.getItem('barang-masuk');
    var model = JSON.parse(data);
    let result = [];
    for (let i = 0; i < model.length; i++) {
        let temp = {
            "id_barang":model[i].id,
            "kuantiti":model[i].kuantiti
        }
        result.push(temp);
    }
    return result;
}

function onSubmit(){
    debugger;
    const model = {
        "tanggal":$('#tanggal').val(),
        "id_supplier":$('#id_supplier').val(),
        "penerima":$('#penerima').val(),
        "items":getSubmittedItem()
    };
    $.ajax({
        url: '/inventori/areas/api/rest.php?page=barang-masuk&action=add',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(model),
        success: function (data) {
            sessionStorage.setItem('barang-masuk','[]');
            alert(data.message);                
            window.location.href = '/inventori?page=barang-masuk';                
        }
    });
}