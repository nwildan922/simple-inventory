(function ($) {
    "use strict";
    var mainApp = {

        onInit: function () {
            $.get( "/inventori/areas/api/rest.php?page=barang-keluar&action=chart", function(data) {
                const model = JSON.parse(data);
                Morris.Donut({
                    element: 'morris-donut-chart',
                    data:model.data,
                    colors: [
                        '#f36a5a','#1ABC9C ','#30A5FF', '#FABE28', 'cyan'
                    ],
                    resize: true
                });
                $('.donut-chart').cssCharts({type:"donut"}).trigger('show-donut-chart');

            });
                   
            $.get( "/inventori/areas/api/rest.php?page=barang-keluar&action=chart-line", function(data) {
                const model = JSON.parse(data);
                Morris.Line({
                    element: 'morris-line-chart',
                    data:model.data,
                    xkey: 'year',
                    ykeys: ['value'],
                    labels: ['Value'],
                    parseTime: false
                }); 
                $('.line-chart').cssCharts({type:"line"});
            });
        },

        initialization: function () {
            mainApp.onInit();
        }
    }

    $(document).ready(function () {
        mainApp.onInit(); 
    });

}(jQuery));
