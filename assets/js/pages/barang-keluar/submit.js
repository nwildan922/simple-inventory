onInit();
    
function onInit(){
    const id = $('#id').val();
    if(Number(id) > 0){
        enableViewOnly();
        getData();
    }else{
        bindData();
        preventDefaultOnEnter();
    }
}
function enableViewOnly(){
    $('#btn_tambah').hide();
    $('#btn_submit').hide();
    $('#bayar').prop("disabled", true );
    $('#th-stok').hide();
}
function getData(){
    debugger;
    const id = $('#id').val();
    $.ajax({
        url: `/inventori/areas/api/rest.php?page=barang-keluar&action=view&id=${id}`,
        type: 'get',
        dataType: 'json',
        contentType: 'application/json',
        success: function (data) {
            if(data.isSuccess){
                bindDataViewOnly(data.data);
            }
        }
    });
}
function bindDataViewOnly(model) {
    const header = model.header;
    $('#total').val(header.total);
    $('#bayar').val(header.bayar);
    $('#kembalian').val(header.kembalian);
    const detail = model.detail;
    $('#data_tbl_barang_keluar').empty();
        for(var i=0;i<detail.length;i++){
            const kode_barang = detail[i].kode_barang;
            const nama_barang = detail[i].nama_barang;
            const harga = detail[i].harga;
            const kuantiti = detail[i].kuantiti;
            const subtotal = detail[i].subtotal;
            $('#data_tbl_barang_keluar').append(
            `<tr>
                <td>${i + 1}</td>
                <td>${kode_barang}</td>
                <td>${nama_barang}</td>
                <td>${harga}</td>
                <td>${kuantiti}</td>
                <td>${subtotal}</td>
                <td>-</td>
            </tr>`
            );
        }
}
function preventDefaultOnEnter(){
    $(window).keydown(function(event){
        if(event.keyCode == 13) {
        event.preventDefault();
        return false;
        }
    });

}

function bindData(){
    debugger;
    var data = sessionStorage.getItem('barang-keluar');
    if(data === undefined || data === null || data === "" || data === "[]" ) {
        addEmptyData();                        
    }else{
        var model = JSON.parse(data);
        $('#data_tbl_barang_keluar').empty();
        for(var i=0;i<model.length;i++){
            const id = model[i].id;
            const kode_barang = model[i].kode_barang;
            const nama_barang = model[i].nama_barang;
            const kuantiti = model[i].kuantiti;
            const stok_kuantiti = model[i].stok_kuantiti;
            const harga = model[i].harga;
            const subtotal = harga * kuantiti;
            $('#data_tbl_barang_keluar').append(
            `<tr>
                <td>${i + 1}<input type='hidden' value='${id}' /></td>
                <td>${kode_barang}</td>
                <td>${nama_barang}</td>
                <td>${harga}</td>
                <td><input id='brg-qty-${i}' type='hidden' value='${stok_kuantiti}'>${stok_kuantiti}</td>
                <td><input id='brg-stok-qty-${i}' type="number" style='width:50px' value='${kuantiti}' onchange='onUpdateKuantiti(${i},this)' /></td>
                <td id='subtotal_${i}'>${subtotal}</td>
                <td>
                    <button type='button' class="btn btn-sm btn-danger" onclick='onRemoveData(${i})'>Hapus</button>
                </td>
            </tr>`
            );
        }
        calculateTotal(model);
    }
}

function addEmptyData (){
    $('#data_tbl_barang_keluar').empty();
    $('#data_tbl_barang_keluar').append(`<tr><td colspan='8' style='text-align:center'>Tidak ada data</td></tr>`);
}

function onRemoveData(index){
    debugger;

    let result = confirm('Apakah anda yakin akan menghapus data ?');

    if(result){
        var data = sessionStorage.getItem('barang-keluar');
        if(data === "" || data === null || data === undefined){
            addEmptyData();
        }else{
            var model = JSON.parse(data);
            model.splice(index,1);
            let modelString = JSON.stringify(model);
            sessionStorage.setItem('barang-keluar',modelString);
            bindData();
            alert('Data berhasil dihapus');
        }
    }else{
        alert('Proses hapus data digagalkan');
    }
}

function onUpdateKuantiti(index,param){
    debugger;
    var data = sessionStorage.getItem('barang-keluar');
    var model = JSON.parse(data);    
    if(param.value <= Number(model[index].stok_kuantiti)){
        model[index].kuantiti = param.value;
        model[index].subtotal = model[index].kuantiti * model[index].harga;
        $(`#subtotal_${index}`).text(model[index].subtotal);
        calculateTotal(model);
    }else{
        alert('Nilai inputan lebih besar dibanding stok');
        model[index].kuantiti = model[index].stok_kuantiti;
        $(`#brg-stok-qty-${index}`).val(model[index].stok_kuantiti);
    }
    let modelString = JSON.stringify(model);
    sessionStorage.setItem('barang-keluar',modelString);
}

function calculateTotal(model){
    let total=0;
    for (let i = 0; i < model.length; i++) {
        const subtotal = model[i].subtotal;
        total += subtotal;            
    }
    $('#total').val(total);
}

function onCalculateKembalian(){
    const total = $('#total').val();
    const bayar = $('#bayar').val();
    const kembalian = bayar - total;
    $('#kembalian').val(kembalian);
}

function getSubmittedItem(){
    var data = sessionStorage.getItem('barang-keluar');
    var model = JSON.parse(data);
    let result = [];
    for (let i = 0; i < model.length; i++) {
        let temp = {
            "id_barang":model[i].id,
            "kuantiti":model[i].kuantiti,
            "harga": model[i].harga
        }
        result.push(temp);
    }
    return result;
}

function onSubmit(){
    debugger;
    const model = {
        "tanggal":$('#tanggal').val(),
        "total":$('#total').val(),
        "bayar":$('#bayar').val(),
        "kembalian":$('#kembalian').val(),
        "items":getSubmittedItem()
    };
    $.ajax({
        url: '/inventori/areas/api/rest.php?page=barang-keluar&action=add',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(model),
        success: function (data) {
            sessionStorage.setItem('barang-keluar','[]');
            alert(data.message);                
            window.location.href = '/inventori?page=barang-keluar';                
        }
    });
}